package com.calculator.modules;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author Witold Dolowicz on 04.01.2021
 */

public class AgreementModule {

    public AgreementModule(WebDriver xyz) {
        driver = xyz;
    }
    private WebDriver driver;

    @Step
    public void clickAgreeButton() {
        WebDriverWait wait = new WebDriverWait(driver, 10, 100);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class=\"sc-ifAKCX ivrbXK\"]")));
        driver.findElement(By.xpath("//button[@class=\"sc-ifAKCX ivrbXK\"]")).click();
        wait.until(ExpectedConditions.numberOfElementsToBe(By.xpath("//button[@class=\"sc-ifAKCX ivrbXK\"]"), 0));
        }
}
