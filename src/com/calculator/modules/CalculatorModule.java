package com.calculator.modules;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author Witold Dolowicz on 04.01.2021
 */


public final class CalculatorModule {


    @FindBy(how = How.ID, using = "input")
    private WebElement inputElement;

    @FindBy(how = How.ID, using = "BtnCalc")
    private WebElement equalsButton;

    @FindBy(how = How.ID, using = "trigorad")
    private WebElement radRadioButton;

    @FindBy(how = How.ID, using = "BtnClear")
    private WebElement clearButton;

    @FindBy(xpath = "//div[@id='hist']/button[2]/span")
    private WebElement historyDropdown;


    @Step
    public void clickEquals() {
        equalsButton.click();
    }

    @Step
    public void clickRadRadio(){
        radRadioButton.click();
    }

    @Step
    public void clickClear() {
        clearButton.click();
    }

    @Step
    public void inputText(String value) {
        inputElement.sendKeys(value);
    }

    @Step
    public void clickHistoryDropdown() {
        historyDropdown.click();
    }

    @Step
    public String inputValue() {
        return inputElement.getText();
    }
}