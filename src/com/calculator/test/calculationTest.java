package com.calculator.test;

import com.calculator.modules.AgreementModule;
import com.calculator.modules.CalculatorModule;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Witold Dolowicz on 04.01.2021
 */

public class calculationTest {

    public WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private CalculatorModule calculatorModule;
    String[] firstEquation = {"34990", "35*999+(100/4)"};
    String[] secondEquation = {"-1", "cos(pi)"};
    String[] thirdEquation = {"9", "sqrt(81)"};

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        String driverPath = "lib/chromedriver";
        System.setProperty("webdriver.chrome.driver", driverPath);
        driver = new ChromeDriver();
        baseUrl = "https://www.google.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        calculatorModule = PageFactory.initElements(driver, CalculatorModule.class);
        driver.get("https://web2.0calc.com/");
        AgreementModule agree = new AgreementModule(driver);
        agree.clickAgreeButton();
    }

    @Test(priority = 1)
    public void mathSequenceTest() throws InterruptedException {
        calculatorModule.inputText(firstEquation[1]);
        calculatorModule.clickEquals();
        assertThat(calculatorModule.inputValue().equals(firstEquation[0]));
        calculatorModule.clickClear();
    }

    @Test(priority = 2)
    public void cosPiTest(){
        calculatorModule.inputText(secondEquation[1]);
        calculatorModule.clickRadRadio();
        calculatorModule.clickEquals();
        assertThat(calculatorModule.inputValue().equals(secondEquation[0]));
        calculatorModule.clickClear();
    }

    @Test(priority = 3)
    public void sqrtTest(){
        calculatorModule.inputText(thirdEquation[1]);
        calculatorModule.clickEquals();
        assertThat(calculatorModule.inputValue().equals(thirdEquation[0]));
        calculatorModule.clickClear();
    }

    @Test(priority = 4)
    public void mathHistoryTest() {
        calculatorModule.clickHistoryDropdown();
        assertThat(driver.findElement(By.xpath("//div[@id='histframe']/ul/li[p[@title='" + firstEquation[0] + "'] and p[@data-inp='" + firstEquation[1] + "']]")).isDisplayed());
        assertThat(driver.findElement(By.xpath("//div[@id='histframe']/ul/li[p[@title='" + secondEquation[0] + "'] and p[@data-inp='" + secondEquation[1] + "']]")).isDisplayed());
        assertThat(driver.findElement(By.xpath("//div[@id='histframe']/ul/li[p[@title='" + thirdEquation[0] + "'] and p[@data-inp='" + thirdEquation[1] + "']]")).isDisplayed());
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        driver.quit();
    }
}